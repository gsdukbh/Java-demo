package top.werls.springboot.actuator.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author leejiawei
 */
@SpringBootApplication
public class SpringBootActuatorDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootActuatorDemoApplication.class, args);
    }

}

